package hello;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "HelloServlet", value = "/hello")
public class HelloServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String display = request.getParameter("display");
        if (display == null || display.trim().equals("")) {
            display = "show";
        }
        if (display.equals("show")) {
            PrintWriter out = response.getWriter();
            out.println(request.getParameter("company"));
            out.println(request.getParameter("year"));
        }
    }
}








//package hello;
//
//import javax.servlet.RequestDispatcher;
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.io.PrintWriter;
//
//
//@WebServlet(name = "HelloServlet", value = "/hello")
//public class HelloServlet extends HttpServlet {
//
//    protected void doPost(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//
//        String display = request.getParameter("display");
//
////        if (display == null) {
////            display = "show";
////        }
//        if (display.equals("show")) {
//            PrintWriter out = response.getWriter();
//            out.println(request.getParameter("company"));
//            out.println(request.getParameter("year"));
//        }
//    }
//
//    @Override
//    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//
//        resp.setContentType("text/html;charset=UTF-8");
//        RequestDispatcher dispatcher = req.getRequestDispatcher("/files/helloServ.jsp");
//        dispatcher.include(req, resp);
//    }
//}