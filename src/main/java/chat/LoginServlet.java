package chat;

import chat.model.UserBean;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(value = "/logchatt")
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        RequestDispatcher dispatcher = req.getRequestDispatcher("/chatjsp/logChat.jsp");
        dispatcher.include(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String user = req.getParameter("user");



        if (user.isEmpty()) {
            RequestDispatcher rD = getServletContext().getRequestDispatcher("/chatjsp/logChat.jsp");

            PrintWriter writer = resp.getWriter();
            writer.println("<font color=red>Either user name!!!</font>");
            rD.include(req, resp);
        } else {
            HttpSession session = req.getSession();
            session.setAttribute(user, "userName");
            Cookie[] cookies = req.getCookies();

            for (Cookie cookie : cookies) {
                if (!cookie.getName().equals(user)) {
                    Cookie userName = new Cookie("user", user);
                    resp.addCookie(userName);
                    break;
                }
            }
            resp.sendRedirect(req.getContextPath() + "/chatjsp/startChat.jsp");
        }
    }

//    public void doGet(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, java.io.IOException {
//
//        try {
//
//            UserBean user = new UserBean();
//            user.setUsername(request.getParameter("un"));
//            user.setPassword(request.getParameter("pw"));
//
//           // user = UserDAO.login(user);
//
//            if (user.isValid()) {
//
//                HttpSession session = request.getSession(true);
//                session.setAttribute("currentSessionUser", user);
//                response.sendRedirect("userLogged.jsp"); //logged-in page
//            } else
//                response.sendRedirect("invalidLogin.jsp"); //error page
//        } catch (Throwable theException) {
//            System.out.println(theException);
//        }
//    }

}   //end class
