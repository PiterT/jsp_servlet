package passwordGenerator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = "/passwordgenerator")
public class PasswordGenerator extends HttpServlet {

    private boolean isSymbols = false;
    private boolean isNumbers = false;
    private boolean isLowerLetters = false;
    private boolean isUpperLetters = false;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        RequestDispatcher dispatcher = req.getRequestDispatcher("/files/generator.jsp");
        dispatcher.include(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String inputSymbol = req.getParameter("inputSymbol");
        String inputNumber = req.getParameter("inputNumber");
        String inputLower = req.getParameter("inputLower");
        String inputUpper = req.getParameter("inputUpper");

        if (inputSymbol != null) {
            isSymbols = true;
        }
        if (inputNumber != null) {
            isNumbers = true;
        }
        if (inputLower != null) {
            isLowerLetters = true;
        }
        if (inputUpper != null) {
            isUpperLetters = true;
        }

    }


}
