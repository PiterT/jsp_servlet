package passwordGenerator;

import java.util.Arrays;
import java.util.Random;

public class StringGenerator {

    private final String SYMBOLS = "!@#$%^&*()_+{}:<>?,./";
    private final String NUMBERS = "0123456789";
    private final String LOWERLETTERS = "qwertyuiopasdfghjklzxcvbnm";
    private final String UPPERLETTERS = LOWERLETTERS.toUpperCase();
    private String ALLCHARS = SYMBOLS + NUMBERS + LOWERLETTERS + UPPERLETTERS;
    private boolean isSymbols = false;
    private boolean isNumbers = false;
    private boolean isLowerLetters = false;
    private boolean isUpperLetters = false;
    private int passwordLength;

    public StringGenerator(boolean isSymbols, boolean isNumbers, boolean isLowerLetters,
                           boolean isUpperLetters, int passwordLength) {
        this.isSymbols = isSymbols;
        this.isNumbers = isNumbers;
        this.isLowerLetters = isLowerLetters;
        this.isUpperLetters = isUpperLetters;
        this.passwordLength = passwordLength;
    }

    public StringGenerator(boolean isSymbols, boolean isNumbers, boolean isLowerLetters, boolean isUpperLetters) {
        this.isSymbols = isSymbols;
        this.isNumbers = isNumbers;
        this.isLowerLetters = isLowerLetters;
        this.isUpperLetters = isUpperLetters;
    }

    public String generateRandomPassword() {
        StringBuffer stringBuffer = new StringBuffer();
        if (isSymbols) {
            int number = new Random().nextInt(SYMBOLS.length());
            String oneSymbol = Arrays.asList(SYMBOLS.split("")).get(number);
            stringBuffer.append(oneSymbol);
        }
        if (isNumbers) {
            int number = new Random().nextInt(NUMBERS.length());
            String oneNumber = Arrays.asList(NUMBERS.split("")).get(number);
            stringBuffer.append(oneNumber);
        }
        if (isLowerLetters) {
            int number = new Random().nextInt(LOWERLETTERS.length());
            String oneLowerLetter = Arrays.asList(LOWERLETTERS.split("")).get(number);
            stringBuffer.append(oneLowerLetter);
        }
        if (isUpperLetters) {
            int number = new Random().nextInt(UPPERLETTERS.length());
            String oneUpperLetter = Arrays.asList(UPPERLETTERS.split("")).get(number);
            stringBuffer.append(oneUpperLetter);
        }

        for (int i = 4; i < passwordLength; i++) {

        }

        return stringBuffer.toString();
    }


}
