package loginpack;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet( value = "/login")
public class Login extends HttpServlet{

    private final String USERADMIN = "admin";
    private final String USERPASSWORD = "password";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        RequestDispatcher dispatcher = req.getRequestDispatcher("/files/logging.jsp");
        dispatcher.include(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String user = req.getParameter("user");
        String password = req.getParameter("password");

        if( USERADMIN.equals(user) && USERPASSWORD.equals(password)){
            resp.sendRedirect(req.getContextPath() + "/files/afterLogging.jsp");
        } else {
            RequestDispatcher rD = getServletContext().getRequestDispatcher("/files/logging.jsp");
            PrintWriter writer = resp.getWriter();
            writer.println("<font color=red>Either user name or password is wrong.</font>");
            rD.include(req, resp);
        }
    }
}
