<%--
  Created by IntelliJ IDEA.
  User: ptoporowski
  Date: 28.05.2018
  Time: 13:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>SECURE PASSWORD GENERATOR</title>
</head>
<body>

<form name="passGenerator" method="post" action="./passwordgenerator">
    <label>Length:</label>
    <select name="length">
        <option>5</option>
        <option>7</option>
        <option>8</option>
        <option>9</option>
    </select>
    <br><label>Include Symbols:</label> <input type="checkbox" name="inputSymbol">( e.g. @#$% )
    <br><label>Include Numbers::</label> <input type="checkbox" name="inputNumber">( e.g. 123456 )
    <br><label>Include LowerCase Characters:</label> <input type="checkbox" name="inputLower">( e.g. abcdefgh )
    <br><label>Include UpperCase Characters:</label> <input type="checkbox" name="inputUpper">( e.g. ABCDEFGH )

    <br>
    <button type="submit" value="Submit">Submit</button>
</form>
</body>
</html>
