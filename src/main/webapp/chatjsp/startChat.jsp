<%@ page import="chat.ReadMessage" %>
<%@ page import="java.util.List" %>
<%@ page import="java.io.IOException" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 29.05.2018
  Time: 19:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Start chat</title>
</head>
<body>
<%
    Cookie[] cookies = request.getCookies();
    Cookie userCookies = null;
    String nameUser = "null";

    for (Cookie cookie : cookies) {
        if (cookie.getName().equals("user")) {
            userCookies = cookie;
            break;
        }
    }
    if (!userCookies.equals(null)) {
        nameUser = userCookies.getValue();
    }
%>
<h1>Hello <%=nameUser%>:</h1>
<%--Dzisiaj jest: <%= java.time.LocalDate.now().toString()%>--%>


<h2>Chat:</h2>

<form action="<%=request.getContextPath()%>/ReadMessage" id="usrform">
    <%--<input type="text" name="message">--%>
    <input type="submit" value="Send message">
</form>
<textarea name="message" form="usrform" rows="10" cols="50">
    Enter text here...
</textarea>

<h2>History: </h2>

<%
    List<String> messages = ReadMessage.messages;
    if (messages.size() > 0) {
        for (int i = 0; i < messages.size(); i++) {
            out.print(messages.get(i));
        }
    }
%>

<form action="LogoutServlet" method="post">
    <input type="submit" value="Logout">
</form>

</body>
</html>

